import React, { useState, useEffect } from "react";
import Button from "@mui/material/Button";
import "./App.css";

function App() {
  const counters = [
    { id: 1, initial: 6, min: -5, max: 10 },
    { id: 2, initial: 5 },
    { id: 3 },
  ];

  const productNames = ["Constructor LEGO", "Train Station", "Hot Wheels"];
  const productPrice = [300, 200, 150];

  return (
    <div className="App">
      <MyCounter initial={0} min={-10} max={10}></MyCounter>
      <MyCounter
        initial={counters[0].initial}
        min={counters[0].counters}
        max={counters[0].max}
      ></MyCounter>

      <MyCounter
        initial={counters[1].initial}
        min={counters[1].counters}
        max={counters[1].max}
      ></MyCounter>
    </div>
  );
}

function MyCounter(props) {
  let [count, setCount] = useState(props.initial);
  useEffect(() => {
    if (count > props.max) {
      setCount(count - 1);
    } else if (count < props.min) {
      setCount(count + 1);
    }
  });

  return (
    <div>
      <p>Поточний рахунок: {count}</p>
      <Button variant="outlined" onClick={() => setCount(count + 1)}>
        +
      </Button>
      &nbsp;
      <Button variant="outlined" onClick={() => setCount(count - 1)}>
        -
      </Button>
      &nbsp;
      <Button variant="outlined" onClick={() => setCount(props.initial)}>
        Reset
      </Button>
    </div>
  );
}

export default App;
